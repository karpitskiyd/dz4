//
//  AppDelegate.swift
//  dz4
//
//  Created by Даниил Карпитский on 2/5/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        let phones:[String:Double] = ["Iphone 8": 30000, "Samsung S20": 50000, "Iphone 12": 60000, "Nexus 6": 60000, "Xiaomi Mi 6": 20000]
        
        
        func filterPhones(phones:[String:Double], filter:(Double)->Bool)
        ->[String:Double]{
            var filteredPhones = [String: Double]()
            for(key, value) in phones{
                if filter(value){
                    filteredPhones[key] = value
                }
            }
         return filteredPhones
        }
        
//        func lowPrice(p:Double)->Bool{
//            if p < 35000{
//                return true
//            }
//            else{
//                return false
//            }
//        }
//        func highPrice(p:Double)->Bool{
//            if p > 40000{
//                return true
//            }
//            else{
//                return false
//            }
//        }
        
        
        
        let lowPricePhones = filterPhones(phones: phones, filter:{p in p < 35000 })
        let highPricePhones = filterPhones(phones: phones, filter:{p in p > 40000 })
        print(lowPricePhones)
        print(highPricePhones)
        
 
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

